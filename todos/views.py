from django.shortcuts import render, redirect
from .models import TodoList
from .forms import TodoForm

# Create your views here.


def todo_list(request):
    todo_list = TodoList.objects.all()
    context = {
        "todo_list": todo_list
    }
    return render(request, "todo/todo.html", context)


def todo_list_detail(request, id):
    todo_list_detail = TodoList.objects.get(id=id)
    context = {
        "todo_object": todo_list_detail
    }
    return render(request, "todo/detail.html", context)


def todo_list_create(request):
    if request.method == "POST":
        form = TodoForm(request.POST)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoForm()

    context = {
        "form": form
    }

    return render(request, "todo/create.html", context)


def todo_list_update(request, id):
    todo_instance = TodoList.objects.get(id=id)
    if request.method == "POST":
        form = TodoForm(request.POST, instance=todo_instance)
        if form.is_valid():
            todo_instance = form.save()
            return redirect("todo_list_detail", id=todo_instance.id)
    else:
        form = TodoForm(instance=todo_instance)
    context = {
        "form": form
    }

    return render(request, "todo/update.html", context)
